---
title: Cara Install Th3Inspector Pada Termux
categories:
     - Tools
tags:
     - Information Gathering
---

Mungkin sebagian dari kalian sudah tidak asing dengan Xattacker, Karna bisa di bilang tools mass auto exploit tersebut cukup populer di kalangan para defacer (pemula)


Selain Xattacker Moham3dRiahi selaku developernya juga membuat sebuah tools Information gathering bernama Th3Inspector dengan segudang fitur yg di sematkan di dalamnya


Biasanya tools information gathering antara satu dan lainya isinya terbilang sama, Mungkin cuma tampilannya saja yg berbeda, Atau bahasa yg di gunakan dalam pembuatanya


Saya tertarik dengan 2 service yg ada pada Th3Inspector ini, Yg pertama adalah bin checker dan yg kedua ialah bypass cloudflare



<img src="https://gitlab.com/lexiie.casanova/gambar/raw/master/Screenshoot/screener_1537668668313.png" width="400" height="400" />



Namun saya masih belum tahu apakah dua hal tersebut bekerja atau tidak, Karna jujur saya sendiri belum sempat mencoba tools ini 


Sebelum ke tutorial cara penginstallan nya ada baiknya saya berikan list fitur dari tools Th3Inspector ini, Apa saja ?


Berikut listnya

- Website Information
- Phone Number Information
- Find IP Address And E-Mail Server
- Domain Whois LookUp
- Find Website/IP Address Location
- Bypass CloudFlare
- Domain Age Checker
- User Agent Info
- Check Active Services On Resource
- Credit Card Bin Checker
- Subdomain Scanner
- Check E-mail Address
- Content Management System Checker


### Cara Install Th3Inspector


Sebelum install siapin dullu package pendukungnya, Hal ini wajib karna jika tidak maka tools tersebut tidak dapat di jalankan 


```bash
$ apt install perl make openssl git
```


Step selanjutnya mendownload atau mengkloning project dari Th3Inspector tersebut dengan perintah berikut


``` bash
$ git clone https://github.com/Moham3dRiahi/Th3inspector
```

Sekarang kita tinggal menginstall module perl yg di butuhkan kan agar tools ini dapat berjalan pada termux kalian


``` bash
$ cpan
$ fforce install HTTP::Request
$ fforce install LWP::UserAgent
$ fforce install JSON
```


Mengapa menggunakan fforce, Ya biar waktu install module ga perlu melakukan testing, Soalnya penginstallan module perl itu terbilang cukup lama


Untuk menjalankan toolsnya cukup mengetik perintah berikut


``` bash
$ perl Th3inspector.pl -argument target
```

Untuk melihat panduan penggunaanya ketik perintah berikut pada terminal


``` bash
$ perl Th3inspector.pl -h
```


