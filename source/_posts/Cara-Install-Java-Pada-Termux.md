---
title: Cara Install Java Termux
categories:
     - Tutorial
---

Mungkin isi artikel ini agak berbeda dengan judulnya yaitu cara install java pada termux, Berbeda dalam arti

Disini saya tidak memberikan tutorial cara install openjdk, Apa itu Openjdk ?

Openjdk adalah sekumpulan package yg berfungsi untuk menginstall semua kebutuhan tools yg di jalankan pada JVM (java virtual machine)

Disini saya lebih ke bagaimana cara mengcompile java bytecode menjadi dex (dalvik excuteable) agar dapat di jalan pada DVM (dalvik virtual machine)

Apa itu DVM ?

DVM adalah sebuah mesin virtual bawaan android yg gunanya untuk menjalankan aplikasi (apk) android itu sendiri

Jika sedikit bingung dengan penjelasan di atas kalian bisa mencari refrensi yg lebih gamblang dan detail tentang penjelasan DVM 

Untuk menjalankan program java di android dengan termux kita harus menginstall package dx dan juga ecj terlebih dahulu

``` bash
$ apt install dx ecj
```

>ecj (eclipe compiler java)
>Berfungsi untu mengcompile code java menjadi class (bytecode).


>dx
>Berfungsi untuk mengcompile class tadi menjadi dex agar dapat di jalankan menggunakan dvm.


Sekarang kita coba buat code javanya, Sebagai contoh kalian bisa memasukan perintah di bawah ini

``` bash
cat > Test.java << EOF
public class Test {
    public static void main(String[] args) {
System.out.println("Gua Gans");
    }
}
EOF
```

Perintah di atas akan menghasilkan sebuah file bernama Test.java, Tahap selanjutnya ialah mengcompile script tadi

``` bash
$ ecj Test.java
```

Setelah di compile akan terdapat file baru yg bernama Test.class, Jika menggunakan Openjdk kita langsung bisa mengeksekusi script tadi dengan perintah `java Test'

Tapi karna tidak ada package openjdk maka kita harus mengcompile class tadi menjadi dex yg nantinya bisa kita jalankan dengan dvm

``` bash
$dx --dex --output=Test.dex Test.class
```

Setelah di compile maka akan terdapat file baru yg bernama Test.dex, Tibalah waktunya untuknya menjalankanya 

``` bash
$ dalvikvm -cp Test.dex Test
```

Setelah di jalankan maka akan terdapat output yg bertuliskan `Gua Gans' , Selamat kalian sudah gans, Tutorialnya mungkin cukup sampai disini saja

Jika terdapat error `failed to initialize runtime` masukan perintah berikut ini untuk mengatasi error tersebut

``` bash
$ sed -i 's/export/#export/g; s/mkdir/#mkdir/g' $PREFIX/bin/dalvikvm
```

Apakah bisa membuat aplikasi android dengan termux ?

Jawabanya sangat sangat bisa jika kalian sudah terbiasa dengan struktur pemerograman pada android 

Adapun package yg di butuhkan untuk membuat aplikasi android dengan termux antara lain

``` bash
$ apt install findutils dx ecj aapt apksigner
```

>findutils
>Digunakan untuk mempermudah proses sortir file java yg jumlahnya tidak sedikit.


>aapt
>Di gunakan untuk melakukan building pada resourse project kita.


>apksigner
>Di gunakan untuk melakukan proses signing pada project kita