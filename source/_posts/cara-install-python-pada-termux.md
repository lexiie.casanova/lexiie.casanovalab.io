---
title: Cara Install Python Pada Termux
categories:
     - Tutorial
tags:
     - Bahasa Pemerograman
---

Python termasuk dalam kategori bahasa pemerograman tingkat tinggi, Oleh karna itu bahasa ini mudah di pelajari

Bahasa python sangat di gemari oleh para pelaku pentest (penetration Tester) karna banyak sekali tools pentest Yg menggunakan bahasa python dalam pembuatan toolsnya

Sebenernya sih ga semua, banyak juga kok web developer yg menggunakan python sebagai backend progamingnya 

Misalnya saja web Codesaya, Berdasarkan keterangan yg di tampilkan pada webnya sendiri menyebutkan jika Codesaya tersebut di bangun dengan bahasa python

Back to topic, Bagaimana sih cara menginstall dan menjalankan python pada termux.

Termux sendiri memiliki 2 varian versi python , Yg pertama python versi 2.7 dan yg kedua python versi 3.6, kita bisa menginstall salah satu atau bisa juga keduanya (sesuai keinginan kalian)

#### Cara Install Python Pada Termux


``` bash
$ pkg install python2
$ pkg install python
```

Pilih salah satu atau keduanya juga boleh 

#### Cara Menjalankan Python Pada Termux


Kita cukup mengetik **python** (python 3.6) **python2** (python 2.7) pada terminal maka setelahnya kita akan langsung masuk ke mode interpreter python


#### Cara Eksekusi Script Python Pada Termux


Untuk mengeksekusi suatu script python, Script yg ingin di jalankan harus menggunakan format .py jika tidak maka akan error

Sebagai percobaan kita akan membuat script python dengan nama test.py yg nantinya akan kita eksekusi, Tulis command berikut pada terminal

``` bash
$ echo "print('Gua Gans')"> test.py
```

Kita sudah membuat scriptnya, Lalu untuk mengeksekusi script tadi masukan perintah berikut

``` bash
$ python test.py
$ python2 test.py
```

Setelah di eksekusi maka akan terdapat output **Gua Gans** pada terminal kalian

#### Package Manager Python Pada Termux

Python memiliki sebuah pustaka library yg dapat kita akses menggunakan package manager pip, Ini sudah otomatis terinstall ketika menginstall python 

Beberapa perintah pip yg dapat kita gunakan sebagai berikut

``` bash
$ pip install namalibrary
$ pip search namalibrary
$ pip show namalibrary
$ pip download namalibrary
$ pip uninstall namalibrary
```

>Install
>untuk menginstall module yg kita inginkan.  


>search 
>untuk mencari suatu module yg ada pada pustaka library python.  


>show 
>untuk menampilkan informasi seputar module.  


>download 
>untuk mendownload module yg nantinya bisa kita install secara lokal, file yg di download extensinya adalah .whl.




