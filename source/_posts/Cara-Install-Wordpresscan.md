---
title: Cara Install Wordpresscan
categories:
    - Tools
---

Wordpresscan adalah project rewrite dari wpscan (ruby) dan terdapat beberapa ide dari wpseku yg di masukan di dalam tools ini 


<img src="
https://gitlab.com/lexiie.casanova/gambar/raw/master/Screenshoot/wordpresscan.png" width="400" height="520" />



Wordpresscan di tulis dengan bahasa python, Lebih tepatnya python versi 2, Tools ini adalah alternatif dari wpscan yg bisa di jalankan pada termux


Karna seperti yg kita ketahui wpscan tidak bisa di install di termux, Karna function dari **getdtablesize** tidak include pada library libc yg ada pada system android


Pihak developer dari wpscan sendiri sudah menyatakan tidak mendukung perangkat mobile dalam salah satu komentar pada halaman issues mereka


Jadi bila kalian menemukan artikel tentang tutorial penginstallan wpscan menggunakan termux,  bisa di abaikan saja karna tidak akan bisa di gunakan


### Cara Install Wordpresscan


``` bash
$ apt install wget
$ wget http://lexiie.web.id/download/wordpresscan_1.0_all.deb
$ apt install -y wordpresscan_1.0_all.deb
```


Proses penginstallan cukup lama terutama waktu proses configurasi dari lxml nya, Silahkan di tunggu hingga selesai


Untuk tutorial penggunaanya sendiri bisa dilihat menggunakan manpage


``` bash
$ pkg install manpage
$ man wordpresscan
```