---
title: Termux Fakeroot
categories:
    - Tutorial
---

Saya buat tools ini untuk pengguna termux yg menggunakan perangkat non root, Karna cukup banyak pengguna yg masih menggunakan perangkat non root


Karna sebagian pengguna berangapan kalo menggunakan termux dengan perangkat non root di rasa kurang leluasa, Padahal kita justru harus berhati hati menggunakan akses root pada termux karna terdapat **Selinux Context**


<img src="
https://gitlab.com/lexiie.casanova/gambar/raw/master/Screenshoot/Fakeroot.png" width="480" height="500" />

Dimana perintah yg di jalankan dengan akses root tidak dapat di jalankan lagi menggunakan perintah non root, Karna permission dari owner dan grup otomatis berubah menjadi root:root


Walaupun sudah ada tutorial untuk cara mengembalikan permissionnya seperti semula, Namun ujung ujungnya kita akan capek juga bila terus terusan mengulangnya


Fungsi termux fakeroot disini ialah mengelabui system agar system beranggapan bahwa kita memiliki akses root padahal tidak demikian


Dengan fakeroot ini kita bisa mengakses system yg apabila di jalankan tanpa user root akan mendapati permission denied, Itu point pertamanya, Point kedua pastinya kita bisa menjalankan tools yg membutuhkan akses root (ex setoolkit)


Apakah fakeroot ini bekerja di luar termux ? 


Tentu saja tidak, Fakeroot hanya berjalan pada aplikasi termux saja, Jadi jangan beranggapan bahwa fakeroot ini seperti supersu 


Karna fakeroot di sini bukan untuk ngeroot android, Tapi lebih ke mengakses system dengan termux, Misalnya saja kita mau edit file build.prop pada system tentu untuk mengeditnya kita harus punya izin supersu dullu


Tapi jika kita mengedit file build.prop dengan termux dan menggunakan tools termux fakeroot ini kita bisa mengedit file tersebut tanpa harus memiliki akses root 


Semoga kalian paham dengan penjelasan di atas agar tidak salah persepsi dengan termux fakeroot ini


### Cara Install Termux Fakeroot


``` bash
$ pkg install wget
$ wget http://lexiie.web.id/Download/Fakeroot_1.1_all.deb
$ apt install -y ./Fakeroot_1.1_all.deb
```


Untuk menjalankanya kalian cukup mengetik **Fakeroot** , kalian bisa menjalankanya pada folder mana saja karna scriptnya excuteable

