---
title: Cara Install Routersploit Pada Termux

categories:
    - Tools
    
tags:
    - exploit
---

Routersploit adalah sebuah framework (router exploitation) yg di tulis dengan bahasa python yg dapat berjalan pada lintas platform (cross-platform)


Setiap code name dari Routersploit selalu mengambil judul lagu dari taylor swift, Versi 1 (wildest dreams), Versi 2 (bad blood), Versi 3 (I knew you where trouble)


<img src="https://gitlab.com/lexiie.casanova/gambar/raw/master/Screenshoot/routersploit.png" width="400" height="520" />


Entah sang developer fans berat dari tayloar swift, Tapi yg pasti pada versi 3 ini terdapat banyak perubahan signifikan terutama dari segi bahasa yg di gunakan


pada versi 1 dan 2 Routersploit menggunakan python versi 2, Untuk yg Versi 3 Routersploit menggunakan python versi 3


Sebelum saya memberikan tutorial installnya ada baiknya saya menjelas dulu fungsi dari tools ini


Routersploit adalah sebuah tools yg di khususkan untuk melakukan exploitation pada suatu router, Jadi jangan salah artikan bahwa Routersploit ini di gunakan untuk membobol jaringan wifi, Karna Routersploit tidak di desain untuk hal tersebut


Apa saja manfaat dari Routersploit 


* Membuat password baru
* Mengganti password saat ini
* Membuat jaringan ssid baru
* Dan masih banyak lagi


### Cara Install Routersploit 


``` bash
$ pkg install wget
$ wget http://lexiie.web.id/download/Routersploit_3_all.deb
$ apt install -y ./Routersploit_3_all.deb
```


Proses penginstallan membutuhkan estimasi waktu sekitar 20-30 menit, Untuk menjalankan tools ini cukup mengetik **rsf** pada terminal


Untuk yg bingung cara menggunakanya bisa menonton video berikut


{% asciinema 180370 %}