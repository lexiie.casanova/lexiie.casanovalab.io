---
title: Perintah Dasar Pada Termux 
categories:  
     - Tutorial
---

Cukup sering saya melihat pengguna baru termux yg malah bingung sendiri sesaat setelah menginstall termux, Pertanyaan yg paling sering dilontar ialah `Setelah menginstall apa yg harus saya lakukan selanjutnya`

Untuk itu disini saya akan coba menulis panduan dasar atau perintah dasar termux, khususnya untuk para pengguna baru termux agar kedepanya bisa lebih mahir lagi dalam menggunakan termux

Perintah termux sendiri tidak jauh berbeda dengan perintah pada GNU/Linux umumnya, karna memang pada dasarnya termux sendiri mengunakan GNU/debian sebagai basis dari Osnya

### Perintah Dasar Termux


``` bash
$ apt install package 
$ pkg install package 
$ apt-get install package 
$ dpkg -i ./package 
```


Perintah di atas fungsinya sama yaitu untuk menginstall package, khusus untuk perintah dpkg di gunakan untuk menginstall lokal package bukan dari repository

``` bash
$ apt update 
$ pkg update 
$ apt-get update 
```

Perintah di atas di gunakan untuk mengupdate package yg terdapat pada repository termux

``` bash
$ apt upgrade 
$ pkg upgrade 
$ apt-get upgrade 
```


Perintah di atas di gunakan untuk mengupgrade semua package yg sudah kita install

``` bash
$ apt update && apt upgrade 
$ apt-get update && apt-get upgrade 
$ pkg up 
```


Perintah di atas di gunakan untuk melakukan update dan upgrade secara bersamaan, Namun perintah di atas hanya berlaku pada bash shell saja, jika di terapkan pada fish atau zsh akan terjadi error

``` bash
$ apt list
$ pkg list-all
```

Perintah di atas di gunakan untuk menampilkan semua package yg terdapat pada repository termux

``` bash
$ apt show
$ pkg show
```

Perintah di atas di gunakan untuk menampilkan informasi suatu package, mulai dari author, size file, depencies, fungsi dari package

``` bash
$ apt remove namapackage
$ pkg uninstall namapackage
```

Perintah di atas di gunakan untuk menghapus suatu package, Kita juga dapat menghapus lebih dari satu package secara bersamaan 

``` bash
$ chsh -s namashell
```

Perintah di atas di gunakan untuk mengganti shell menjadi primary shell, Misalnya shell default termux kan bash dengan perintah di atas kita bisa ubah shell default tadi menjadi zsh/fish/xonsh dll.

``` bash
$ cd namafolder 
$ cd ../ 
```

Perintah di atas di gunakan untuk berpindah folder/directory, Sedang perintah titik titik di gunakan untuk kembali pada folder sebelumnya

``` bash
$ cat namafile 
$ cat folder/namafile 
```

Perintah di atas di gunakan untuk menampilkan isi dari suatu file tanpa harus menggunakan teks editor

``` bash
$ mv namafile foldertujuan/ 
$ mv namafile namabarufile 
```


Perintah di atas di gunakan untuk memindah suatu file/folder, Gunakan peritah (/) apabila ingin memidahkan file, perintah di atas juga bisa di gunakan merename (menamai ulang) suatu file


``` bash
$ rm namafile 
$ rm file1 file2 file3 
$ rm * 
$ rm -rf namafolder 
$ rmdir namafolder 
```

Perintah di atas di gunakan untuk menghapus file, file yg di hapus bisa satu persatu, bisa banyak file, serta semua file (*), Untuk menghapus file beserta foldernya gunakan perintah -rf (recursive folder) atau bisa juga rmdir

``` bash
$ cp namafile foldertujuan/ 
$ cp namafolder foldertujuan/ 
```

Perintah di atas di gunakan untuk mengcopy/menyalin suatu file atau bisa juga folder

``` bash
$ mkdir namafolder 
```

Perintah di atas digunakan untuk membuat sebuah folder atau directory baru

``` bash
$ id
```

Perintah di atas di gunakan untuk menampilkan nama pengguna dan nama grup pada perangkat kita

``` bash
$ pwd
```

Perintah di atas untuk menampilkan directory atau folder tempat kita berada saat ini 

``` bash
$ uname -a
```

Perintah di atas di gunakan untuk menampilkan informasi device atau perangkat yg kita gunakan meliputi nama os, versi kernel, versi arsitektur


Mungkin cukup itu dulu sebagai dasarnya, jika sudah terbiasa kalian bisa mencari refrensi lagi tentang perintah perintah pada GNU/Linux, Karna seperti yg saya katakan pada awal tulisan perintah termux tidak jauh berbeda dengan GNU/Linux pada umumnya


